package gaia.cu9.ari.gaiaorbit.util;

public interface ChecksumRunnable {
    
    public void run(String checksum);

}
